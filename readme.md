# Docker ROS2 Franka-Foxy

## Installation

```bash
docker compose build
docker compose up -d
```

To allow X11 windows to pass-through, on the host machine run:

```
xhost +local:docker
```

## Getting Started

Example:
```bash
docker exec -it ros-foxy bash

```
