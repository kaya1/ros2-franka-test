#!/bin/bash

cd ~/ros2_ws

# source ros
source /opt/ros/foxy/setup.bash

# Run the main container command
exec "$@"
